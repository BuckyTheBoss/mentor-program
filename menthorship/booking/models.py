from django.db import models
from mentor.models import Mentor
from oleh.models import Oleh


class Booking(models.Model):
    STATUS = [
		('pending', 'The match request is pending'),
		('accepted', 'The match request has been accepted'),
        ('rejected', 'The match request has been rejected'),
        ('canceled', 'The match request has been canceled'),
	]
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)
    oleh = models.ForeignKey(Oleh, on_delete=models.CASCADE)
    status = models.CharField(max_length=100, choices=STATUS, default='pending')
    
    def __str__(self):
        return "{} {}".format(self.mentor, self.oleh)