from django.shortcuts import render, redirect
from .models import Booking
from django.contrib.auth.decorators import login_required
from messaging.models import Chat
from mentor.models import Mentor


def create_booking(request, mentor_id):
    mentor = Mentor.objects.get(pk=mentor_id)
    oleh = request.user.oleh
    new_booking = Booking(mentor=mentor, oleh=oleh)
    new_booking.save()
    chat = Chat()
    chat.save()
    chat.users.add(oleh.user, mentor.user)
    return redirect('oleh:dashboard')


def accept_booking(request, booking_id):
    booking = Booking.objects.get(pk=booking_id)
    booking.status = 'accepted'
    booking.save()
    return redirect('mentor:dashboard', mentor_id=booking.mentor_id)
        
def reject_booking(request, booking_id):
    booking = Booking.objects.get(pk=booking_id)
    booking.status = 'rejected'
    booking.save()
    return redirect('mentor:dashboard', mentor_id=booking.mentor_id)
        
def cancel_booking(request, booking_id):
    booking = Booking.objects.get(pk=booking_id)
    booking.status = 'canceled'
    booking.save()
    return redirect('mentor:dashboard', mentor_id=booking.mentor_id)
