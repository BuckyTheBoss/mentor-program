from django.urls import path
from . import views

app_name = 'booking'

urlpatterns = [
  path('create/<int:mentor_id>', views.create_booking, name='create_booking'),
  path('reject/<int:booking_id>', views.reject_booking, name='reject_booking'),
  path('cancel/<int:booking_id>', views.cancel_booking, name='cancel_booking'),
  path('accept/<int:booking_id>', views.accept_booking, name='accept_booking')
]