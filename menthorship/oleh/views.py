from django.shortcuts import render, redirect
from .models import Oleh, Task
from .forms import OlehForm, OlehIntakeForm
from main.models import Event
from mentor.models import Mentor
from messaging.models import Chat, Message


def show_oleh(request, oleh_id):
    oleh = Oleh.objects.get(pk=oleh_id)
    return render(request, 'oleh.html', {
        'oleh': oleh
    })
              
def show_olehs(request):
    olehs = Oleh.objects.all()
    return render(request, 'olehs.html', {
        'oleh': olehs
    })              

def delete_oleh(request, oleh_id):
    oleh = Oleh.objects.get(pk=oleh_id).delete()
    return redirect('oleh:show_olehs')
    
def add_oleh(request):
    form = OlehForm()
    if request.method == 'POST':
        form = OlehForm(request.POST, request.FILES, instance=oleh)
        if form.is_valid():
            form.save()
            return redirect('oleh:show_olehs')
    return render(request, 'add_oleh.html', {
        'form': form
    })
    
def edit_oleh(request, oleh_id):
    oleh = Oleh.objects.get(pk=oleh_id)
    form = OlehForm(instance=oleh)
    if request.method == 'POST':
        form = OlehForm(request.POST, request.FILES, instance=oleh)
        if form.is_valid():
            form.save()
            return redirect('oleh:show_oleh', oleh_id=oleh_id)
    return render(request, 'edit_oleh.html', {
        'oleh': oleh,
        'form': form
    })

def intake_oleh(request):
  oleh = request.user.oleh
  form = OlehIntakeForm(instance=oleh)
  if request.method == "POST":
    form = OlehIntakeForm(request.POST, request.FILES, instance=oleh)
    if form.is_valid():
      form.save()
      return redirect('oleh:pick_mentor')
  return render(request, 'intake_oleh.html', {
      'oleh': oleh,
      'form': form
  })

def dashboard(request):
  chat = Chat.objects.filter(users=request.user).distinct().first()
  if request.method == "POST":
    message = Message(chat=chat, content=request.POST.get('content'), sender=request.user)
    message.save()
  events = Event.objects.filter(location=request.user.oleh.location)
  tasks = Task.objects.filter(oleh=request.user.oleh)
  return render(request, 'oleh_dashboard.html', {'events' : events, 'tasks' : tasks, 'chat' : chat})

def pick_mentor(request):
  oleh = request.user.oleh
  mentor_list = Mentor.objects.filter(languages=oleh.languages.first()).filter(location=oleh.location)
  return render(request, 'list_of_mentors.html', {'mentor_list' : mentor_list} )