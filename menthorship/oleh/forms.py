from django.forms import ModelForm
from .models import Oleh


class OlehForm(ModelForm):
    class Meta:
        model = Oleh
        fields = '__all__'

class OlehIntakeForm(ModelForm):
    class Meta:
        model = Oleh
        fields = ['location', 'description', 'languages']
