from django.db import models
from django.contrib.auth.models import User
from main.models import Language, Location



class Oleh(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	description = models.TextField(blank=True, null=True)
	location = models.ForeignKey(Location, on_delete=models.CASCADE, null=True)
	picture = models.ImageField(upload_to='images', null=True)
	languages = models.ManyToManyField(Language)

	def __str__(self):
		return "{}".format(self.user)


class Task(models.Model):
	oleh = models.ForeignKey(Oleh, on_delete=models.CASCADE)
	value = models.CharField(max_length=220)
