from django.urls import path
from . import views

app_name = 'oleh'

urlpatterns = [
    path('oleh/<int:oleh_id>', views.show_oleh, name='show_oleh'),
    path('show_all', views.show_olehs, name='show_olehs'),
    path('delete/<int:oleh_id>', views.delete_oleh, name='delete_oleh'),
    path('edit_oleh/<int:oleh_id>', views.edit_oleh, name='edit_oleh'),
    path('add_oleh', views.add_oleh, name='add_oleh'),
    path('intake_oleh',views.intake_oleh, name='intake_oleh'),
    path('dashboard',views.dashboard, name='dashboard'),
    path('pick_mentor',views.pick_mentor, name='pick_mentor'),
]