from django.shortcuts import render
from django.contrib.auth.models import User

# Create your views here.

def chat(request,user_id):
	user2 = User.objects.filter(pk=user_id).first()
	chat = Chat.objects.filter(users=request.user).filter(users=user2).distinct().first()

	if chat == None:
		chat = PrivateChat()
		chat.save()
		chat.users.add(request.user.profile, user2)
	if request.method == 'POST':
		message = Message(chat=chat, content=request.POST.get('content'), profile=request.user.profile)
		message.save()
	return render(request, 'chat.html', {'chat' : chat, 'user2' : user2})


def chat_by_id(request,chat_id):
	chat = Chat.objects.filter(pk=chat_id).first()
	user2 = chat.users.exclude(user=request.user).first()
	return redirect('chat', user2.id )
