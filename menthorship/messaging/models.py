from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Chat(models.Model):
	users = models.ManyToManyField(User)

class Message(models.Model):
	content = models.TextField(blank=True, null=True)
	timestamp = models.DateTimeField(auto_now_add=True)
	sender = models.ForeignKey(User, on_delete=models.CASCADE)
	chat = models.ForeignKey(Chat, on_delete=models.CASCADE)