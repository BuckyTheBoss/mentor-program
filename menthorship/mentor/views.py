from django.shortcuts import render, redirect
from .models import Mentor
from booking.models import Booking
from .forms import MentorForm, MentorIntakeForm
from booking.views import *


def show_mentor(request, mentor_id):
    mentor = Mentor.objects.get(pk=mentor_id)
    return render(request, 'mentor_dashboard.html', {
        'mentor': mentor
    })
              
def show_mentors(request):
    mentors = Mentor.objects.all()
    return render(request, 'mentors.html', {
        'mentor': mentors
    })              

def delete_mentor(request, mentor_id):
    mentor = Mentor.objects.get(pk=mentor_id).delete()
    return redirect('mentor:show_mentors')
    
def add_mentor(request):
    form = MentorForm()
    if request.method == 'POST':
        form = MentorForm(request.POST, request.FILES, instance=mentor)
        if form.is_valid():
            form.save()
            return redirect('mentor:show_mentors')
    return render(request, 'add_mentor.html', {
        'form': form
    })
    
def edit_mentor(request, mentor_id):
    mentor = Mentor.objects.get(pk=mentor_id)
    form = MentorForm(instance=mentor)
    if request.method == 'POST':
        form = MentorForm(request.POST, request.FILES, instance=mentor)
        if form.is_valid():
            form.save()
            return redirect('mentor:show_mentor', mentor_id=mentor_id)
    return render(request, 'edit_mentor.html', {
        'mentor': mentor,
        'form': form
    })

def intake_mentor(request):
    mentor = request.user.mentor
    form = MentorIntakeForm(instance=mentor)
    if request.method == "POST":
        form = MentorIntakeForm(request.POST, request.FILES, instance=mentor)
        if form.is_valid():
            form.save()
        return redirect('mentor:show_mentor', mentor_id=mentor.id)
    return render(request, 'intake_mentor.html', {
        'mentor': mentor,
        'form': form
    })

def dashboard(request, mentor_id):
    mentor = Mentor.objects.get(pk=mentor_id)
    bookings = Booking.objects.filter(mentor_id=mentor.id)
    return render(request, 'mentor_dashboard.html', {
        'mentor': mentor,
        'bookings': bookings
    })