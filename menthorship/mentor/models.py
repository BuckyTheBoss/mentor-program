from django.db import models
from django.contrib.auth.models import User
from main.models import Language, Location


class Mentor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, default=None, null=True)
    status = models.BooleanField(default=False)
    description = models.TextField(null=True, default=None)
    picture = models.ImageField(upload_to='images', null=True)
    languages = models.ManyToManyField(Language)

    def __str__(self):
        return "{}".format(self.user)
