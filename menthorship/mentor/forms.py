from django.forms import ModelForm
from .models import Mentor
from booking.models import Booking


class MentorForm(ModelForm):
    class Meta:
        model = Mentor
        fields = '__all__'


class MentorIntakeForm(ModelForm):
    class Meta:
        model = Mentor
        fields = ['location', 'description', 'picture', 'languages']
        