from django.urls import path
from . import views

app_name = 'mentor'

urlpatterns = [
    path('mentor/<int:mentor_id>', views.show_mentor, name='show_mentor'),
    path('show_all', views.show_mentors, name='show_mentors'),
    path('delete/<int:mentor_id>', views.delete_mentor, name='delete_mentor'),
    path('edit_mentor/<int:mentor_id>', views.edit_mentor, name='edit_mentor'),
    path('add_mentor', views.add_mentor, name='add_mentor'),
    path('intake_mentor',views.intake_mentor, name='intake_mentor'),
    path('dashboard/<int:mentor_id>',views.dashboard, name='dashboard'),
]