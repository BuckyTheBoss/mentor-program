from django.db import models

# Create your models here.

class Language(models.Model):
	name = models.CharField(max_length=15)
	def __str__(self):
		return "{}".format(self.name) 

class Location(models.Model):
	name = models.CharField(max_length=15)
	def __str__(self):
		return "{}".format(self.name)

class Event(models.Model):
	name = models.CharField(max_length=50)
	location = models.ForeignKey(Location, on_delete=models.CASCADE)
	datetime = models.DateTimeField()
	picture = models.URLField(null=True, default=None)