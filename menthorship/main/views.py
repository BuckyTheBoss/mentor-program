from django.shortcuts import render, redirect
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from oleh.models import Oleh
from mentor.models import Mentor

# Create your views here.
def signup_error(request, message=None):
  if message is None:
    message = 'Sorry, something went wrong, please try again.'
  
  messages.add_message(request, messages.ERROR, message)
  return redirect('signup')

def signup(request):
  if request.method == 'POST':
    # password validation
    password = request.POST.get('password')
    try: 
      validate_password(password, user=User)
    except:
      return signup_error(request=request, message='Password did not meet minimum security requirements.')

    # username validation
    username = request.POST.get('email')
    if User.objects.filter(username=username).exists():
      return signup_error(request=request, message='This email address is already taken.')

    # name validation
    first_name = request.POST.get('first_name')
    last_name = request.POST.get('last_name')
    if len(first_name) == 0:
      return signup_error(request=request, message='Please enter a first name.')
    if len(last_name) == 0:
      return signup_error(request=request, message='Please enter a last name.')


    # user member creation
    user = User.objects.create_user(
      first_name=first_name,
      last_name=last_name,
      username=username,
      email=username,
      password=password
    )
    user.save()

    if authenticate(username=username, password=password) is None:
      return signup_error(request=request)

    login(request, user)
    # detecting user type
    user_type = request.POST.get('user_type')
    if user_type == 'oleh':
      profile = Oleh(user=user)
      profile.save()
      return redirect('oleh:intake_oleh')
    else:
      profile = Mentor(user=user)
      profile.save()
      return redirect('mentor:intake_mentor')


  return render(request, 'signup.html')
  
def login_view(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(username=email, password=password)
        if user is not None:
            login(request, user)
            try:
                mentor = request.user.mentor
                return redirect('mentor:show_mentor', mentor_id=request.user.mentor.id)
            except:
                oleh = request.user.oleh
                return redirect('oleh:show_oleh', oleh_id=request.user.oleh.id)
        
    return render(request, 'login.html')
    
def logout_view(request):
    logout(request)
    return render(request, 'logout.html')