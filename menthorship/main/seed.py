from .models import *
from oleh.models import *
from faker import Faker
from django.contrib.auth.models import User
from mentor.models import Mentor
from oleh.models import Oleh
import random

fake = Faker()


def gen_fname():
	return fake.first_name()

def gen_lname():
	return fake.last_name()

def gen_password():
	return fake.password(length=10, special_chars=True, digits=True, upper_case=True, lower_case=True)

def gen_user():
	return random.choice(User.objects.all())

def gen_title():
	return fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)

def gen_paragraph():
	return fake.paragraph(nb_sentences=5, variable_nb_sentences=True, ext_word_list=None)

def seed_languages():
	language_names = ['English', 'Russian', 'French', 'Dutch', 'German', 'Italian']

	for language_name in language_names:
		language = Language(name=language_name)
		language.save()

def seed_locations():
	location_names = ['Haifa', 'Tel Aviv', 'Jerusalem']
	for location_name in location_names:
		location = Location(name=location_name)
		location.save()

def gen_language():
	return random.choice(Language.objects.all())

def gen_location():
	return random.choice(Location.objects.all())

def create_mentors(number):
	for i in range(0, number):
		fname = gen_fname()
		lname = gen_lname()
		username = fname.lower() + lname.lower()
		user = User.objects.create_user(username=username, password=gen_password(), first_name=fname, last_name=lname)
		user.save()
		mentor = Mentor(user=user, location=gen_location(), description=gen_paragraph())
		mentor.save()
		mentor.languages.add(gen_language())

def create_olehs(number):
	for i in range(0, number):
		fname = gen_fname()
		lname = gen_lname()
		username = fname.lower() + lname.lower()
		user = User.objects.create_user(username=username, password=gen_password(), first_name=fname, last_name=lname)
		user.save()
		oleh = Oleh(user=user, location=gen_location(), description=gen_paragraph())
		oleh.save()
		oleh.languages.add(gen_language())



def seed_event_tasks():

	event_names = ['Hackathon for Olim','Streetball','Jerusalem Tech Job Fair','Tel Aviv Tech Job Fair','Haifa Tech Job Fair','Haifa Singles Mixer']
	event_location_pks = ['2','3','3','2','1','1']
	evet_datetimes = ['2019-10-10 13:30','2019-11-11 13:30','2019-10-10 13:30','2019-11-11 13:30','2019-10-10 13:30','2019-11-11 13:30']
	event_pic_url = ['https://hackernoon.com/hn-images/1*AXF8IYKqC3Y7JxYRaUrCPQ.png','https://image.shutterstock.com/image-vector/colorful-logo-streetball-against-brick-260nw-1168062832.jpg','http://tbrnewsmedia.com/wp-content/uploads/2019/03/job-fair.jpg','http://tbrnewsmedia.com/wp-content/uploads/2019/03/job-fair.jpg','http://tbrnewsmedia.com/wp-content/uploads/2019/03/job-fair.jpg','http://catchmatchmaking.com/wp-content/uploads/2014/08/shutterstock_147796814.jpg']


	for n in range(0,6):
		event = Event(name=event_names[n], location=Location.objects.get(pk=event_location_pks[n]), datetime=evet_datetimes[n],picture=event_pic_url[n])
		event.save()

	task_list = ['Army follow-up', 'Pick a health insurance provider', 'Order 1st Israeli passport', 'Set up bank meeting for misgeret ashrai', 'go to side project tuesday']
	for oleh in Oleh.objects.all():
		for task_name in task_list:
			task = Task(oleh=oleh, value=task_name)
			task.save()



